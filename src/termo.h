#ifndef TERMO_H_
#define TERMO_H_

#include <iostream>
#include <cstring>
#include <string>
#include <exception>
#include "termo.h"
#include "temporizador.h"

using std::string;
using std::cout;
using std::endl;


class Termo
{
	public:
		Termo(string termo, unsigned int tamanho);
		Termo(char* termo, unsigned int tamanho);
		~Termo();
		char* getPalavra() const;
		int comparar(Termo* other) const;
	private:
		char* palavra;
		unsigned int tamanho;
};

#endif /* TERMO_H_ */
