#ifndef HASH_SIMPLES_H_
#define HASH_SIMPLES_H_

#include <iostream>

#include "debug.h"
#include "temporizador.h"
#include "arvore_avl.h"
#include "resultado_busca.h"

using namespace std;


class IndiceInvertido
{
	public:
		IndiceInvertido(int tamanhoTabelam, int tamanhoPalavra);
		~IndiceInvertido();
		void insere(OcorrenciaTermo* ocorrenciaTermo);
		ResultadoBusca* busca(string palavra);
		void imprimir() const;
		void imprimirPalavras(bool imprime) const;
	private:
		ArvoreAVL** tabela;
		ListaArquivo* baseArquivos;
		int numeroArquivos;
		int tamanhoTabela;
		int tamanhoPalavra;
		int totalOcorrencias;
		int totalTermosDistintos;
		unsigned int hash(const char *string) const;
		ArvoreAVL** iniciaTabelaHash(int tamanhoTabela);
		ListaOcorrencia* buscaHash(Termo* t);
		void addOcorrencia(bool novo);
		void registrarOcorrencia(bool novo, OcorrenciaTermo* ocorrenciaTermo);
		void addArquivo(Arquivo* arquivo);
		void adicionarNaArvoreDeOrdenacao(bool novo, OcorrenciaTermo* aux);
		bool insereOcorrenciaEmIndice(unsigned int indice, OcorrenciaTermo* ocorrenciaTermo);
};

#endif
