#ifndef _DEBUG_H
#define _DEBUG_H
#include <cstdlib>
#include <cstdio>

//#define DEBUG 1
#ifdef DEBUG



#define INFO(format, ...) fprintf(stderr,"INFO : %s:%d:%s -> " format "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)
#define LOG(format, ...) fprintf(stderr,"TRACE: %s:%d:%s -> " format "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)

#else

#define LOG(format, ...)
#define INFO(format, ...)

#endif

#define ERROR(format, ...) fprintf(stderr,"ERROR: %s:%d:%s -> " format "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__); exit(-1)


#endif
