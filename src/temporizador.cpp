#include "temporizador.h"
#define EMPTY -1.0

Temporizador etapas[] = {
		Temporizador(CRIAR_INDICE_INVERTIDO, 0, "Criar indice invertido"),
		Temporizador(LEITURA_ARQUIVO, 1, "Leitura arquivo"),
		Temporizador(PARSE_PALAVRA, 2, "Parse de palavra"),
		Temporizador(INSERE_HASH, 0, "Insere Indice Invertido"),
		Temporizador(CALCULO_HASH, 3, "Calculo do hash"),
		Temporizador(INSERE_ARVORE, 3, "Insere AVL"),
		Temporizador(INSERE_ARVORE_PALAVRAS_ORDENADAS, 3, "Insere Arvore de palavras ordenadas"),
		Temporizador(COMPARA_TERMOS, 4, "Compara termos"),
		Temporizador(BALANCEAMENTO_ARVORE, 4, "Balanceamento AVL"),
		Temporizador(BUSCA_INDICE_INVERTIDO, 0, "Busca no indice invertido"),
		Temporizador(BUSCA_ARVORE, 1, "Busca na Arvore AVL"),
		Temporizador(LISTA_PALAVRAS, 0, "Lista ordenada"),
		Temporizador(FIM,-1,"")
};

Temporizador::Temporizador(Etapa id, int level, const char* titulo)
{
	Temporizador::id = id;
	Temporizador::level = level;
	Temporizador::titulo = titulo;
	Temporizador::ultimo_inicio = EMPTY;
	Temporizador::tempo_total = 0;
	Temporizador::menor_tempo = EMPTY;
	Temporizador::maior_tempo = EMPTY;
	Temporizador::numero_chamadas = 0;
}

Temporizador::~Temporizador()
{
	// Adeus!
}

void Temporizador::inicio()
{
	if (ultimo_inicio == EMPTY)
	{
		ultimo_inicio = clock();
	}
	else
	{
		ERROR("Iniciando Etapa ja iniciada: %s", titulo);
	}
}

void Temporizador::fim()
{
	if (ultimo_inicio != EMPTY)
	{

		clock_t fim = clock();
		double tempo = ((double) (fim - ultimo_inicio)) / CLOCKS_PER_SEC;

		if (menor_tempo  == EMPTY || tempo < menor_tempo)
			menor_tempo = tempo;

		if (maior_tempo  == EMPTY || tempo > maior_tempo)
			maior_tempo = tempo;

		tempo_total += tempo;
		ultimo_inicio = EMPTY;
		numero_chamadas++;
	}
	else
	{
		ERROR("Finalizando Etapa nao iniciada: %s", titulo);
	}
}

double Temporizador::getTempoMedio() const
{
	double media = 0.0;
	if (numero_chamadas > 0)
		media = tempo_total/numero_chamadas;
	return media;
}

double Temporizador::getMenorTempo() const {
	double menorTempo =  menor_tempo;
	if (menorTempo == EMPTY)
		menorTempo = 0;
	return menorTempo;
}

double Temporizador::getMaiorTempo() const {
	double maiorTempo =  maior_tempo;
	if (maiorTempo == EMPTY)
		maiorTempo = 0;
	return maiorTempo;
}

void Temporizador::imprimir() const
{
	for (int i =0; i < (level); i++) cout << ".";
	cout << setiosflags(ios::fixed) << setprecision(12) << setw(15) << tempo_total;
	cout << "   |";
	cout << setiosflags(ios::fixed) << setprecision(12) << setw(15) <<getMenorTempo();
	cout << "   |";
	cout << setiosflags(ios::fixed) << setprecision(12) << setw(15) <<getTempoMedio();
	cout << "   |";
	cout << setiosflags(ios::fixed) << setprecision(12) << setw(15) <<getMaiorTempo();
	cout << "   |";
	cout << setfill(' ') << setw(12) << numero_chamadas;
	cout << "   | " << titulo;
}

void imprimirEtapas()
{
	 cout << endl << endl;
	 cout << "----------------------------------------------------------" << endl;
	 cout << "Tempos" << endl;
	 cout << "----------------------------------------------------------" << endl;
	 cout << "Tempo Total(ms)| Menor tempo(ms) | Tempo medio(ms) | Maior Tempo(ms) | Chamadas | Etapa";
	 int idx_etapas = 0;
	 while(etapas[idx_etapas].getId() != FIM) {
		 cout << endl;
		 etapas[idx_etapas].imprimir();
		 idx_etapas++;
	 }
	 cout << endl;
}

void imprimirEtapasTestes()
{
	 cout << endl << endl;
	 cout << "----------------------------------------------------------" << endl;
	 cout << "Tempos" << endl;
	 cout << "----------------------------------------------------------" << endl;
	 cout << "Tempo Total(ms)| Menor tempo(ms) | Tempo medio(ms) | Maior Tempo(ms) | Chamadas | Etapa";
	 cout << endl;
	 etapas[3].imprimir();
	 cout << endl;
	 etapas[9].imprimir();
	 cout << endl;
	 etapas[11].imprimir();
	 cout << endl;
}

