#include "menu.h"

using namespace std;

/**
* Metodo main da aplicacao
*/
int main_(int argc, char *argv[])
{
    // Pega o tamanho da palavra do hash passado como parametro para a aplicacao
    int tamanhoPalavra = 20;
    if (argv[1] != NULL && atoi(argv[1]) > 20){
        tamanhoPalavra = atoi(argv[1]);
    }

	//Classe Menu
	Menu aux;
	aux.executar(tamanhoPalavra);

    return 0;
}
