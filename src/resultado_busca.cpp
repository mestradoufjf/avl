#include "resultado_busca.h"


ListaResultadoArquivo *merge_sort_list(ListaResultadoArquivo *list);


ListaTermoBuscado::ListaTermoBuscado(Termo* t, double p, ListaTermoBuscado* px)
{
	termo = t;
	pesoNoDocumento = p;
	prox = px;
}

ListaResultadoArquivo::ListaResultadoArquivo(Arquivo* arquivo)
{
	ListaResultadoArquivo::arquivo = arquivo;
	ListaResultadoArquivo::termos = NULL;
	ListaResultadoArquivo::prox = NULL;
	ListaResultadoArquivo::pesoAcumulado = 0.0;
	ListaResultadoArquivo::relevancia = -1.0;

}

void ListaResultadoArquivo::addTermo(Termo* termo, double pesoTermo)
{
	pesoAcumulado += pesoTermo;

	termos = new ListaTermoBuscado(termo, pesoTermo, termos);

	relevancia = -1.0;
}

double ListaResultadoArquivo::getRelevancia()
{
	if (relevancia < 0)
	{
		relevancia = pesoAcumulado / arquivo->getTotalDistintas();
	}

	return relevancia;
}

ResultadoBusca::ResultadoBusca(ListaTermoOcorrencia* termoOcorrencias, unsigned int tamanhoBaseArquivos)
{
	arquivos = NULL;
	tamanho = 0;

	double logN = log(tamanhoBaseArquivos);
	double pesoTermoINoArquivoJ = 0.0;

	ListaTermoOcorrencia* i = termoOcorrencias;

	while (i != NULL)
	{
		ListaOcorrencia* j = i->getOcorrencias();
		unsigned int numeroDeDocumentosEmQueTermoOcorre = j->getTamanho();
		while (j != NULL)
		{
			Ocorrencia* ocorrencia = j->getOcorrencia();
			unsigned int numeroDeOcorrenciasNoArquivo = ocorrencia->getContagem();
			pesoTermoINoArquivoJ =  numeroDeOcorrenciasNoArquivo * logN / numeroDeDocumentosEmQueTermoOcorre;
			this->add(ocorrencia->getArquivo(), i->getTermo(), pesoTermoINoArquivoJ);

			j = j->getProx();
		}

		i = i->getProx();
	}


	arquivos = merge_sort_list(arquivos);
}

void ResultadoBusca::add(Arquivo* arquivo, Termo* termo, double peso)
{

	ListaResultadoArquivo* it = arquivos;

	if (arquivos != NULL)
	{
		while (it != NULL && it->getArquivo()->getId() != arquivo->getId())
		{
			it = it->getProx();
		}
	}

	if (it == NULL)
	{
		it = arquivos;
		arquivos = new ListaResultadoArquivo(arquivo);
		arquivos->addTermo(termo, peso);
		arquivos->setProx(it);

		tamanho++;
	}
	else
	{
		it->addTermo(termo, peso);
	}
}

ListaResultadoArquivo* ResultadoBusca::getResultado() const
{
	return arquivos;
}


/**
 * http://www.chiark.greenend.org.uk/~sgtatham/algorithms/listsort.html
 *
 */
ListaResultadoArquivo *merge_sort_list(ListaResultadoArquivo *list)
{
    int listSize=1,numMerges,leftSize,rightSize;
    ListaResultadoArquivo *tail,*left,*right,*next;

    // Caso trivial
    if (!list || !list->getProx())
    	return list;

    // Para cada potencia de 2 <= tamanho da lista
    do {
    	numMerges=0,left=list;tail=list=0; // Comecando do comeco

        while (left) { // Do this list_len/listSize times:
            numMerges++,right=left,leftSize=0,rightSize=listSize;
            // Cut list into two halves (but don't overrun)
            while (right && leftSize<listSize) leftSize++,right=right->getProx();
            // Run through the lists appending onto what we have so far.
            while (leftSize>0 || (rightSize>0 && right)) {
                // Left empty, take right OR Right empty, take left, OR compare.
                if (!leftSize)                  {next=right;right=right->getProx();rightSize--;}
                else if (!rightSize || !right)  {next=left;left=left->getProx();leftSize--;}
                else if ((left->getRelevancia() - right->getRelevancia())>0) {next=left;left=left->getProx();leftSize--;}
                else                            {next=right;right=right->getProx();rightSize--;}
                // Update pointers to keep track of where we are:
                if (tail) tail->setProx(next);  else list=next;
                // Sort prev pointer
                tail=next;
            }
            // Right is now AFTER the list we just sorted, so start the next sort there.
            left=right;
        }
        // Terminate the list, double the list-sort size.
        tail->setProx(NULL);
        listSize<<=1;
    } while (numMerges>1); // If we only did one merge, then we just sorted the whole list.
    return list;
}
