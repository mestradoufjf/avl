#ifndef ARQUIVO_H_
#define ARQUIVO_H_

#include <string>

using namespace std;

class Arquivo {
public:
	Arquivo(int id, string nome);
	virtual ~Arquivo();
	void addPalavra(bool nova);
	int getId() const {return id;};
	string getNome() const {return nome;};
	int getTotalPalavras() const {return totalPalavras;};
	int getTotalDistintas() const {return totalDistintas;};
private:
	int id;
	string nome;
	int totalPalavras;
	int totalDistintas;
};


class ListaArquivo {
public:
	ListaArquivo(Arquivo* a, ListaArquivo* l) {arquivo = a; prox = l;};
	~ListaArquivo() {delete arquivo; if (prox != NULL) delete prox;};
	Arquivo* getArquivo() const {return arquivo;};
	ListaArquivo* getProx() const {return prox;};
	bool add(Arquivo* arquivo);
private:
	Arquivo* arquivo;
	ListaArquivo* prox;
};

#endif
