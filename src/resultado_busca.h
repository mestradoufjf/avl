/*
 * resultado_busca.h
 *
 *  Created on: Jun 1, 2014
 *      Author: developer
 */

#ifndef RESULTADO_BUSCA_H_
#define RESULTADO_BUSCA_H_

#include <cmath>
#include "temporizador.h"
#include "arvore_avl.h"

class ListaTermoBuscado
{
	public:
		ListaTermoBuscado(Termo* t, double p, ListaTermoBuscado* px);
		~ListaTermoBuscado() { if (termo != NULL) delete termo; if (prox != NULL) delete prox;};
		Termo* getTermo() const {return termo;};
		double getPesoNoDocumento() const { return pesoNoDocumento;};
		ListaTermoBuscado* getProx() const { return prox;};
	private:
		Termo* termo;
		double pesoNoDocumento;
		ListaTermoBuscado* prox;
};

class ListaResultadoArquivo
{
public:
	ListaResultadoArquivo(Arquivo* arquivo);
	~ListaResultadoArquivo() { if (termos != NULL) delete termos; if (prox != NULL) delete prox;};
	Arquivo* getArquivo() const {return arquivo;}
	void addTermo(Termo* t, double peso);
	void setProx(ListaResultadoArquivo* p)  { prox = p;};
	ListaTermoBuscado* getTermos() const { return termos;};
	ListaResultadoArquivo* getProx() const { return prox;};
	double getRelevancia();
private:
	Arquivo* arquivo;
	ListaTermoBuscado* termos;
	double pesoAcumulado;
	double relevancia;
	ListaResultadoArquivo* prox;
};

class ResultadoBusca
{
	public:
		ResultadoBusca(ListaTermoOcorrencia* termoOcorrencias, unsigned int tamanhoBaseArquivos);
		~ResultadoBusca() {if (arquivos != NULL) delete arquivos;};
		unsigned int getTamanho() const {return tamanho;};
		ListaResultadoArquivo* getResultado() const;
	private:
		void add(Arquivo* arquivo,  Termo* termo, double peso);
		ListaResultadoArquivo* arquivos;
		unsigned int tamanho;
};

#endif /* RESULTADO_BUSCA_H_ */
