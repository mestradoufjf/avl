#include "leitor_arquivo.h"
#define EMPTY_CHAR -1

/**
 * Construtor
 */
LeitorArquivo::LeitorArquivo(int tamanhoPalavra) {
	LeitorArquivo::totalPalavras = 0;
	LeitorArquivo::tamanhoPalavra = tamanhoPalavra;
	LeitorArquivo::tamanhoHash = 2011;
}

/**
 * Construtor
 */
LeitorArquivo::LeitorArquivo(int tamanhoPalavra, int tamanhoHash) {
	LeitorArquivo::totalPalavras = 0;
	LeitorArquivo::tamanhoPalavra = tamanhoPalavra;
	LeitorArquivo::tamanhoHash = tamanhoHash;
}

/**
 * Destrutor
 */
LeitorArquivo::~LeitorArquivo() {
}

/**
 * Lê arquivos a partir do arquivo de entrada
 */
IndiceInvertido* LeitorArquivo::ler(const char* caminhoArquivo) {
	INICIO_ETAPA(CRIAR_INDICE_INVERTIDO);
	IndiceInvertido* indiceInvertido = new IndiceInvertido(tamanhoHash, tamanhoPalavra);
	ifstream arq;
	string linha;

	arq.open(caminhoArquivo);
	if (arq.is_open() && arq.good()) {
		int nroLinha = 0;
		int nroMaximo = 0;
		while (!arq.eof()) {
			if (nroLinha > nroMaximo) {
				break;
			}

			getline(arq, linha);

			if (nroLinha == 0) {
				nroMaximo = atoi(linha.c_str());
			} else {
				if (linha.size() > 0) {
					lerArquivo(new Arquivo(nroLinha, linha), indiceInvertido);
				}
			}

			nroLinha++;
		}
	} else {
		ERROR("Nao foi possivel ler o arquivo de entrada. \"%s\"",
				caminhoArquivo);
	}
	arq.close();

	INFO("Total de palavras lidas a partir do arquivo descritor %s = %d):" , caminhoArquivo, totalPalavras);

	FIM_ETAPA(CRIAR_INDICE_INVERTIDO);
	return indiceInvertido;
}

/**
 * Le os arquivos passados a partir do entrada.txt
 */
void LeitorArquivo::lerArquivo(Arquivo* arquivo, IndiceInvertido* tabelaHash) {
	cout << "Indexando arquivo " << arquivo->getNome() << endl;
	INICIO_ETAPA(LEITURA_ARQUIVO);

	char* palavra;
	FILE* arquivo_entrada;

	arquivo_entrada = fopen(arquivo->getNome().c_str(), "r");
	if (arquivo_entrada) {
		palavra = getPalavra(arquivo_entrada);
		while (palavra) {
			tabelaHash->insere(new OcorrenciaTermo(new Termo(palavra, tamanhoPalavra), arquivo));
			LeitorArquivo::totalPalavras++;
			palavra = getPalavra(arquivo_entrada);
		}
		fclose(arquivo_entrada);
	}

	FIM_ETAPA(LEITURA_ARQUIVO);
}

/**
 *
 * Busca as palavra no arquivo passado como parametro
 */
char* LeitorArquivo::getPalavra(FILE* arquivo) {
	INICIO_ETAPA(PARSE_PALAVRA)
	unsigned int __tamanho_palavra = 0, i;
	char caracter_atual;
	char *palavra;

	palavra = (char*) calloc(sizeof(char), LeitorArquivo::tamanhoPalavra+1);
	if (!palavra){
		return NULL;
	}

	caracter_atual = fgetc(arquivo);

	while (!isalpha(caracter_atual) && caracter_atual != EMPTY_CHAR){
		caracter_atual = fgetc(arquivo);
	}

	if (isalpha(caracter_atual)) {
		palavra[__tamanho_palavra] = tolower(caracter_atual);
		__tamanho_palavra++;
		caracter_atual = tolower(fgetc(arquivo));

		while (isalnum(caracter_atual)) {
			palavra[__tamanho_palavra] = caracter_atual;
			__tamanho_palavra++;

			caracter_atual = tolower(fgetc(arquivo));

			if (__tamanho_palavra == LeitorArquivo::tamanhoPalavra) {
				// Descartando os caracteres alem do limite
				while (isalnum(caracter_atual)) {
					caracter_atual = fgetc(arquivo);
				}
			}
		}
	}

	if (__tamanho_palavra > 0)
	{
		for (i = __tamanho_palavra; i < LeitorArquivo::tamanhoPalavra; i++) {
			palavra[i] = ' ';
		}
		palavra[LeitorArquivo::tamanhoPalavra] = '\0';
	}
	else
	{
		free(palavra);
		palavra = NULL;
	}

	FIM_ETAPA(PARSE_PALAVRA)
	return palavra;
}



