/*
 * Menu.h
 *
 *  Created on: May 24, 2014
 *      Menu Prncipal da Aplicacao
 */

#ifndef MENU_H_
#define MENU_H_

#include <iostream>
#include <string>
#include "leitor_arquivo.h"
#include "temporizador.h"
#include <stdlib.h>


using namespace std;

class Menu {
public:
	Menu();
	virtual ~Menu();
	void executar(int tamanhoPalavra) ;
private:
	void criaIndiceInvertido(int tamanhoPalavra, IndiceInvertido** IndiceInvertido);
	void imprimeOpcoes() const;
	bool indiceInvertidoInicializado(IndiceInvertido* indiceInvertido) const;
	void imprimeIndiceInvertido(IndiceInvertido* IndiceInvertido) const;
	void busca(IndiceInvertido* indiceInvertido);
};

#endif /* MENU_H_ */
