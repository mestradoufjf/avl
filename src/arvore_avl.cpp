#include "arvore_avl.h"

#define ALTURA(x) (x != NULL ? x->getAltura() : -1)

using std::max;

OcorrenciaTermo::OcorrenciaTermo(Termo* termo, Arquivo* arquivo){
	OcorrenciaTermo::termo = termo;
	OcorrenciaTermo::arquivo = arquivo;
}

OcorrenciaTermo::~OcorrenciaTermo(){
}

OcorrenciaTermo* OcorrenciaTermo::clone() const {
	return new OcorrenciaTermo(termo, arquivo);
}

Ocorrencia::Ocorrencia(Arquivo* arquivo)
{
	Ocorrencia::arquivo = arquivo;
	Ocorrencia::contagem = 1;
}

void Ocorrencia::inc()
{
	contagem++;
}

ListaOcorrencia::ListaOcorrencia(Arquivo* arquivo)
{
	ocorrencia = new Ocorrencia(arquivo);
	prox = NULL;
	tamanho = 1;
}

ListaOcorrencia::~ListaOcorrencia()
{
	delete ocorrencia;
	if (prox != NULL)
	{
		delete prox;
	}
}

void ListaOcorrencia::add(Arquivo* arquivo)
{
	if (arquivo != NULL)
	{
		ListaOcorrencia* it = this;
		ListaOcorrencia* ultimo = this;
		while (it != NULL) {
			if (it->ocorrencia->getArquivo()->getId() == arquivo->getId()) {
				it->ocorrencia->inc();
				return;
			}
			ultimo = it;
			it = it->prox;
		}

		ultimo->prox  = new ListaOcorrencia(arquivo);
		tamanho++;
	}
	else
	{
		ERROR("O arquivo nao pode ser nulo. %s", "");
	}
}

No::No(OcorrenciaTermo* ocorrenciaTermo)
{
	No::termo = ocorrenciaTermo->getTermo();
	No::ocorrencias = new ListaOcorrencia(ocorrenciaTermo->getArquivo());
	No::esquerda = NULL;
	No::direita = NULL;
	No::altura = 0;
	delete ocorrenciaTermo;
}

No::~No()
{
	delete termo;
	delete ocorrencias;
}

void No::add(OcorrenciaTermo* ocorrenciaTermo)
{
	ocorrencias->add(ocorrenciaTermo->getArquivo());
	delete ocorrenciaTermo->getTermo();
	delete ocorrenciaTermo;

}

void No::setEsquerda(No* no)
{
	esquerda = no;
	atualizaAltura();
}

void No::setDireita(No* no) {
	direita = no;
	atualizaAltura();
}

void No::atualizaAltura() {
	altura = 1 + max(ALTURA(esquerda), ALTURA(direita));
}

ArvoreAVL::ArvoreAVL()
{
	raiz = NULL;
}

bool ArvoreAVL::insere(OcorrenciaTermo* ocorrenciaTermo)
{
	bool novo = false;

	raiz = insereNo(raiz, ocorrenciaTermo, novo);

	return novo;
}

/*
 * Insere Ocorrência na Árvore
 */
No* ArvoreAVL::insereNo(No *raiz, OcorrenciaTermo* ocorrenciaTermo, bool& novo )
{
   if (raiz == NULL)
    {
        raiz = new No(ocorrenciaTermo);
        novo = true;
        return raiz;
    }
    else
    {
		Termo* termo = ocorrenciaTermo->getTermo();

		if (termo->comparar(raiz->getTermo()) == 0)
		{
			novo = false;
			raiz->add(ocorrenciaTermo);
		}
		else if (termo->comparar(raiz->getTermo())  < 0)
		{
			raiz->setEsquerda(insereNo(raiz->getEsquerda(), ocorrenciaTermo, novo));
			raiz = balancear (raiz);
		}
		else if (termo->comparar(raiz->getTermo())  > 0)
		{
			raiz->setDireita(insereNo(raiz->getDireita(), ocorrenciaTermo, novo));
			raiz = balancear (raiz);
		}
    }

    return raiz;
}


/*
 * Altura da árvore
 */
int ArvoreAVL::altura(No *temp)
{
    int h = 0;
    if (temp != NULL)
    {
        int aEsquerda = altura (temp->getEsquerda());
        int aDireita = altura (temp->getDireita());
        int max_height = max (aEsquerda, aDireita);
        h = max_height + 1;
    }
    return h;
}

int ArvoreAVL::getAltura() const
{
	int  altura = 0;
	if (raiz != NULL)
	{
		altura = 1 + raiz->getAltura();
	}
	return altura;
}


/*
 * Diferença de altura
 */
int ArvoreAVL::diff(No *temp)
{

	int fatorBalanceamento = 0;

		if (temp != NULL)
		{
			fatorBalanceamento =  ALTURA(temp->getEsquerda()) - ALTURA (temp->getDireita());
		}

	return  fatorBalanceamento;
}

/*
 * Rotação Simples à Direita
 */
No *ArvoreAVL::rotacaoSimplesAEsquerda(No *pai)
{
	No *temp;
    temp = pai->getEsquerda();
    pai->setEsquerda(temp->getDireita());
    temp->setDireita(pai);
    return temp;
}
/*
 * Rotação Simples à Esquerda
 */
No* ArvoreAVL::rotacaoSimplesADireita(No* pai)
{
	No *temp;
	temp = pai->getDireita();
	pai->setDireita(temp->getEsquerda());
	temp->setEsquerda(pai);

	return temp;
}

/*
 * Rotação Esquerda-Direita
 */
No* ArvoreAVL::rotacaoEsquerdaDireita(No *pai)
{

    No *temp = pai->getEsquerda();
    pai->setEsquerda(rotacaoSimplesADireita(temp));
    return rotacaoSimplesAEsquerda(pai);
}


/*
 * Rotação Direita-Esquerda
 */
No* ArvoreAVL::rotacaoDireitaEsquerda(No *pai)
{
    No *temp = pai->getDireita();
    pai->setDireita(rotacaoSimplesAEsquerda(temp));
    return rotacaoSimplesADireita(pai);
}

/*
 * Balanceamento da AVL
 */
No *ArvoreAVL::balancear(No *temp)
{
	INICIO_ETAPA(BALANCEAMENTO_ARVORE);
    int fatorBalanceamento = diff (temp);
    if (fatorBalanceamento > 1)
    {
        if (diff (temp->getEsquerda()) > 0)
            temp = rotacaoSimplesAEsquerda(temp);
        else
            temp = rotacaoEsquerdaDireita(temp);
    }
    else if (fatorBalanceamento < -1)
    {
        if (diff (temp->getDireita()) > 0)
            temp = rotacaoDireitaEsquerda(temp);
        else
            temp = rotacaoSimplesADireita (temp);
    }
    FIM_ETAPA(BALANCEAMENTO_ARVORE);
    return temp;
}

ListaOcorrencia* ArvoreAVL::busca(Termo* t) const
{
	INICIO_ETAPA(BUSCA_ARVORE);
	ListaOcorrencia* ocorrencias = NULL;
	No* no = raiz;

	while (no != NULL)
	{
		int comparacao = t->comparar(no->getTermo());
		if (comparacao == 0)
		{
			ocorrencias = no->getOcorrencias();
			break;
		}
		else if (comparacao > 0)
		{
			no = no->getDireita();
		}
		else if (comparacao < 0)
		{
			no = no->getEsquerda();
		}
	}
	 FIM_ETAPA(BUSCA_ARVORE);

	return ocorrencias;
}

void ArvoreAVL::imprimir() const
{
	cout << endl;
	imprimir(raiz, 1);
}

void ArvoreAVL::imprimir(int recuo) const
{
	cout << endl;
	imprimir(raiz, recuo);
}

void ArvoreAVL::imprimir(No* no, int nivel) const
{
	if (no != NULL) {
		cout << " [\"" << no->getTermo()->getPalavra() << "\" h="<<no->getAltura();
		ListaOcorrencia* ocorrencia = no->getOcorrencias();
		while (ocorrencia != NULL) {

			cout << " (arquivo=" << ocorrencia->getOcorrencia()->getArquivo()->getId()<<", ";
			cout << "ocrrencias=" << ocorrencia->getOcorrencia()->getContagem()<<") ";
			ocorrencia = ocorrencia->getProx();
		}
		cout << "]" << endl;

		for (int i =0; i < nivel-1; i++) cout << "  ";

		cout << "ESQ";
		imprimir(no->getEsquerda(), nivel + 2);
		cout << endl;

		for (int i =0; i < nivel-1; i++) cout << "  ";
		cout << "DIR";
		imprimir(no->getDireita(), nivel + 2);
	} else {
		for (int i =0; i < nivel-1; i++) cout << "  ";
		cout << "NULL";
	}
}


void ArvoreAVL::imprimirPalavras(bool imprime) const
{
	imprimirPalavras(raiz, imprime);
}

void ArvoreAVL::imprimirPalavras(No* no, bool imprime) const
{
	if (no != NULL) {
		imprimirPalavras(no->getEsquerda(), imprime);
		if (imprime)
			cout << no->getTermo()->getPalavra() << endl;
		imprimirPalavras(no->getDireita(), imprime);
	}
}
