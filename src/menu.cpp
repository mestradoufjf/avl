/*
 * Menu.cpp
 *
 *  Created on: May 24, 2014
 *      Menu Principal da Aplicacao
 */

#include "menu.h"

/**
 * Construtor
 */
Menu::Menu()
{
}

/**
 * Destrutor
 */
Menu::~Menu()
{
}

void Menu::criaIndiceInvertido(int tamanhoPalavra, IndiceInvertido** indiceInvertido)
{
	// Escapando memory leak caso o usuario crie 2 vezes o indice
	if (*indiceInvertido != NULL)
	{
		delete *indiceInvertido;
		cout << "Recriando indice invertido...";
	}



	LeitorArquivo arq = LeitorArquivo(tamanhoPalavra);
	*indiceInvertido = arq.ler("entradas/entrada.txt");
}

void Menu::imprimeIndiceInvertido(IndiceInvertido* indiceInvertido) const
{
	indiceInvertido->imprimir();
}

void Menu::imprimeOpcoes() const
{
	cout << endl << endl;
	cout << "================ Menu Principal =================" << endl;
	cout << " Selecione a opcao abaixo:" << endl;
	cout << " (1) Criar indice invertido" << endl;
	cout << " (2) Buscar por palavra" << endl;
	cout << " (3) Inserir Palavra" << endl;
	cout << " (4) Listar palavras alfabeticamente" << endl;
	cout << " (5) Exbir tempo de processamento das operacoes" << endl;
	cout << " (6) Imprimir indice invertido" << endl;
	cout << " (0) Sair" << endl;
	cout << "================================================" << endl;
	cout << ">> " << endl;
}

/*
 * Cria o Menu
 */
void Menu::executar(int tamanhoPalavra) {

	int opcao;
	IndiceInvertido* indiceInvertido = NULL;


	do
	{

		imprimeOpcoes();

		cin >> opcao;

		switch (opcao)
		{
			case 0 :
				break;
			//Criar indice invertido
			case 1: Menu::criaIndiceInvertido(tamanhoPalavra, &indiceInvertido);
				break;

			//Busca indice  invertido
			case 2:
				if (indiceInvertidoInicializado(indiceInvertido))
					busca(indiceInvertido);

				break;

		    // Listar palavras alfabeticamente
			case 4:
				if (indiceInvertidoInicializado(indiceInvertido))
					indiceInvertido->imprimirPalavras(true);
				break;

			// Listar resultados
			case 5:IMPRIME_ETAPAS();
				break;

				// Imprime indice
			case 6:
				if (indiceInvertidoInicializado(indiceInvertido))
					indiceInvertido->imprimir();
				break;

			default:
				cout << "Opcao invalida, digite outra opcao ou 0 para sair." << endl;
		}
	} while (opcao != 0);

	if (indiceInvertido != NULL)
	{
		delete indiceInvertido;
	}
}
void Menu::busca(IndiceInvertido* indiceInvertido) {

	cout << "Informe o termo a ser buscado: " << endl;
	std::cin >> std::ws;
	std::string palavra;
	std::getline(std::cin, palavra);

	ResultadoBusca* resultadoBusca = indiceInvertido->busca(palavra);
	int totalResultados = resultadoBusca->getTamanho();
	if ( totalResultados == 0)
	{
		cout << "Nenhuma ocorrencia para a palavra '" << palavra << "' foi encontrada :(" << endl;
	}
	else
	{
		cout << "Busca por '" << palavra << "':" << endl;
		cout << "-----------------------------------------";

		ListaResultadoArquivo* arquivos = resultadoBusca->getResultado();
		ListaResultadoArquivo* it = arquivos;
		int resultados = 0;
		while (it != NULL)
		{
			cout << endl << "Arquivo: " << it->getArquivo()->getNome() << "(relevancia "<< it->getRelevancia()<<")";
			ListaTermoBuscado* termoBuscado = it->getTermos();
			while(termoBuscado != NULL)
			{
				cout << endl<< "\t" << termoBuscado->getTermo()->getPalavra() << "(" << termoBuscado->getPesoNoDocumento()<<")";
				termoBuscado = termoBuscado->getProx();
			}

			it = it->getProx();
			cout << "---" << endl;
			if (++resultados % 5 == 0)
			{
				cout << "Pagina "<< (resultados/5) << "/ " << ceil(totalResultados/5) << endl;
				cin.ignore();
			}
		}
		cout << endl << "-----------------------------------------" << endl;
		cout << "Total: " << totalResultados << " Arquivos" << endl;
		cin.ignore();

		delete resultadoBusca;
	}

}

bool Menu::indiceInvertidoInicializado(IndiceInvertido* indiceInvertido) const
{
	bool inicializado = indiceInvertido != NULL;
	if (!inicializado)
		cout << "E necessario criar o indice antes de realizar essa operacao." << endl;
	return inicializado;
}


