#include "termo.h"
#include <stdlib.h>

/**
 * Esse construtor e sensivel, a palavra DEVE ter o tamanho informado.
 */
Termo::Termo(char* palavra, unsigned int tamanho)
{
	if (palavra != NULL) {
		Termo::palavra = palavra;
		Termo::tamanho = tamanho;
	} else {
        cout << "Termo::Termo(char* palavra) > Palavra não pode ser nula!" << endl;
	}
}

Termo::Termo(string termo, unsigned int tamanho)
{
	unsigned int i = 0;
	if (tamanho > 0)
	{
		const char* caracteresTermo = termo.c_str();
		char* palavra = (char*) malloc(sizeof(char)* tamanho+1);
		char caractere;

		while (i <  termo.npos && i < tamanho)
		{
			caractere = tolower(caracteresTermo[i]);
			if (isalnum(caractere)){
				palavra[i] = caractere;
				i++;
			}
			else
			{
				break;
			}
		}

		while (i < tamanho)
		{
			palavra[i] = ' ';
			i++;
		}

		palavra[tamanho] = '\0';
		Termo::palavra = palavra;
		Termo::tamanho = tamanho;
	}
	else
	{
		ERROR("O tamanho de uma palavra deve ser maior que 0, %d nao e um valor valido.", tamanho);
	}
}

Termo::~Termo()
{
	if (palavra != NULL)
		free(palavra);
}

char* Termo::getPalavra() const
{
	return palavra;
}

int Termo::comparar(Termo* other) const
{
	INICIO_ETAPA(COMPARA_TERMOS);

	unsigned int i = 0;
	while (i < tamanho && i < other->tamanho && palavra[i] == other->palavra[i] && palavra[i]!=' ') {
		i++;
	}
	int comparacao = palavra[i] - other->palavra[i];

	FIM_ETAPA(COMPARA_TERMOS);
	return comparacao;

}

