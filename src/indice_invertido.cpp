#include "indice_invertido.h"
#undef get16bits
#if (defined(__GNUC__) && defined(__i386__)) || defined(__WATCOMC__) \
  || defined(_MSC_VER) || defined (__BORLANDC__) || defined (__TURBOC__)
#define get16bits(d) (*((const uint16_t *) (d)))
#endif

#if !defined (get16bits)
#define get16bits(d) ((((uint32_t)(((const uint8_t *)(d))[1])) << 8)\
                       +(uint32_t)(((const uint8_t *)(d))[0]) )
#endif

#define HASHWORDBITS 32

IndiceInvertido::IndiceInvertido(int tamanhoTabela, int tamanhoPalavra)
{
	IndiceInvertido::tabela = iniciaTabelaHash(tamanhoTabela);
	IndiceInvertido::baseArquivos = NULL;
    IndiceInvertido::tamanhoTabela = tamanhoTabela;
    IndiceInvertido::tamanhoPalavra = tamanhoPalavra;
    IndiceInvertido::totalOcorrencias = 0;
    IndiceInvertido::totalTermosDistintos = 0;
    IndiceInvertido::numeroArquivos = 0;
}

IndiceInvertido::~IndiceInvertido()
{
	for (int i = 0; i < tamanhoTabela; i++)
	{
		if (tabela[i] != NULL)
		{
			delete tabela[i];
		}
	}
	delete tabela;
}

ArvoreAVL** IndiceInvertido::iniciaTabelaHash(int tamanhoTabela) {
	ArvoreAVL** tabela = (ArvoreAVL**) (malloc(sizeof(ArvoreAVL*) * tamanhoTabela + 1));
	for (int i = 0; i < tamanhoTabela +1; i++) {
		tabela[i] = NULL;
	}
	return tabela;
}

void IndiceInvertido::addOcorrencia(bool novo)
{
	totalOcorrencias++;
	if (novo)
		totalTermosDistintos++;
}

void IndiceInvertido::addArquivo(Arquivo* arquivo) {
	if (baseArquivos == NULL) {
		baseArquivos = new ListaArquivo(arquivo, NULL);
		numeroArquivos = 1;
	}
	else
	{
		if (baseArquivos->add(arquivo))
		{
			numeroArquivos++;
		}
	}
}

void IndiceInvertido::registrarOcorrencia(bool novo, OcorrenciaTermo* ocorrenciaTermo)
{
	Arquivo* arquivo = ocorrenciaTermo->getArquivo();
	arquivo->addPalavra(novo);

	addArquivo(arquivo);
	addOcorrencia(novo);
}

void IndiceInvertido::adicionarNaArvoreDeOrdenacao(bool novo, OcorrenciaTermo* ocorrencia) {
	if (novo) {
		insereOcorrenciaEmIndice(tamanhoTabela + 1, ocorrencia);
	} else {
		delete ocorrencia;
	}
}

bool IndiceInvertido::insereOcorrenciaEmIndice(unsigned int indice,	OcorrenciaTermo* ocorrenciaTermo)
{
	if (tabela[indice] == NULL) {
		tabela[indice] = new ArvoreAVL();
	}
	// Insere na hash
	bool novo = tabela[indice]->insere(ocorrenciaTermo);

	return novo;
}

void IndiceInvertido::insere(OcorrenciaTermo* ocorrenciaTermo)
{
	INICIO_ETAPA(INSERE_HASH);
	const char* palavra = ocorrenciaTermo->getTermo()->getPalavra();

	LOG("Inserindo ocorrência do termo %s no arquivo %d", palavra, ocorrenciaTermo->getArquivo());

	unsigned int indice = hash(palavra);

	LOG("Hash para o termo \"%s\": %d", palavra, indice);

	// Clonando para insercao na arvore de 'memoria' para ordenacao
	OcorrenciaTermo *aux = ocorrenciaTermo->clone();

	// Inserindo no indice calculado da hash
	INICIO_ETAPA(INSERE_ARVORE);
	bool novo = insereOcorrenciaEmIndice(indice, ocorrenciaTermo);
	FIM_ETAPA(INSERE_ARVORE);


	// Contabilizar nos totalizadores
	registrarOcorrencia(novo, ocorrenciaTermo);

	INICIO_ETAPA(INSERE_ARVORE_PALAVRAS_ORDENADAS);
	// Armazena o termo na arvore de palavras ordenadas
	adicionarNaArvoreDeOrdenacao(novo, aux);
	FIM_ETAPA(INSERE_ARVORE_PALAVRAS_ORDENADAS);

	FIM_ETAPA(INSERE_HASH);
}

ListaOcorrencia* IndiceInvertido::buscaHash(Termo* t)
{
	ListaOcorrencia* ocorrencias = NULL;

	if (t != NULL)
	{
		ArvoreAVL* avl = tabela[hash(t->getPalavra())];
		if (avl != NULL)
		{
			ocorrencias = avl->busca(t);
		}
	}

	return ocorrencias;
}

ResultadoBusca* IndiceInvertido::busca(string termosBusca)
{
	INICIO_ETAPA(BUSCA_INDICE_INVERTIDO);

	ListaTermoOcorrencia* termoOcorrencias = NULL;

		unsigned int i = 0;
		unsigned int ultimoInicio = 0;
		const char* caracteres = termosBusca.c_str();
		unsigned int tamanhoPalavra = strlen(caracteres);

		while (i <= tamanhoPalavra)
		{
			// Gera palavra a cada inicio de palavra ou no fim dos termos
			if (!isalnum(caracteres[i]) || i == tamanhoPalavra)
			{
				string termoBusca = termosBusca.substr(ultimoInicio, i - ultimoInicio);
				Termo* termo = new Termo( termoBusca, IndiceInvertido::tamanhoPalavra);
				ListaOcorrencia* ocorrencias = buscaHash(termo);

				if (ocorrencias != NULL)
					termoOcorrencias = new ListaTermoOcorrencia(termo, ocorrencias, termoOcorrencias);
				else
					delete termo;

				// Descartando espacos em branco
				while (!isalnum(caracteres[i]) && i <= tamanhoPalavra)
					i++;

				ultimoInicio = i;
			} else {
				i++;
			}
		}

	ResultadoBusca* resultado = new ResultadoBusca(termoOcorrencias, numeroArquivos);

	FIM_ETAPA(BUSCA_INDICE_INVERTIDO);

	return resultado;
}

void IndiceInvertido::imprimir() const
{
	cout << "[TABELAHASH tamanhoTabela="<< tamanhoTabela<<"]" ;
	for (int i = 0; i < tamanhoTabela; i++)
	{
		if (tabela[i] != NULL)
		{
			cout << endl << "[Índice "<< i << " ----------------------------]";

			tabela[i]->imprimir(2);
		}
	}
	cout << endl;
	cout << "-------------------------------------------------------------"
		 << "Total ocorrencias "<< totalOcorrencias<<", numero de termos distintos" << totalTermosDistintos
		 << "-------------------------------------------------------------";

}

void IndiceInvertido::imprimirPalavras(bool imprime) const
{
	cout << "[LISTAGEM EM ORDEM ALFABETICA]" << endl;

	INICIO_ETAPA(LISTA_PALAVRAS)
	tabela[tamanhoTabela+1]->imprimirPalavras(imprime);
	FIM_ETAPA(LISTA_PALAVRAS)

	cout << "-------------------------------------------------------------" << endl
		 << "Total de palavras " << totalOcorrencias << endl
		 << "-------------------------------------------------------------" << endl;

}



/* Defines the so called `hashpjw' function by P.J. Weinberger
   [see Aho/Sethi/Ullman, COMPILERS: Principles, Techniques and Tools,
   1986, 1987 Bell Telephone Laboratories, Inc.]  */
unsigned int IndiceInvertido::hash (const char *strparam) const
{
  INICIO_ETAPA(CALCULO_HASH);
  unsigned int hval, g;
  const char *str = strparam;

  /* Compute the hash value for the given string.  */
  hval = 0;
  while (*str != '\0')
    {
      hval <<= 4;
      hval += (unsigned char) *str++;
      g = hval & ((unsigned int) 0xf << (HASHWORDBITS - 4));
      if (g != 0)
	{
	  hval ^= g >> (HASHWORDBITS - 8);
	  hval ^= g;
	}
    }
  unsigned int hashIdx = hval % tamanhoTabela;
  FIM_ETAPA(CALCULO_HASH);

  return hashIdx;
}

