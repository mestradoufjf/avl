#include <iostream>
#include <cstdlib>
#include "debug.h"
#include "leitor_arquivo.h"
#include "temporizador.h"

using namespace std;

char* termos[] = {"englush",
		"hipopotomonstrossesquipedaliofobico",
		"jaiminho",
		"Pneumoultramicroscopicossilicovulcanoconiose",
		"arvorepatricia",
		"Gutenberg",
		"several",
		"to",
		"newspaper",
		"asterisk",
		"To create these etexts",
		"quem com ferro fere com ferro sera ferido",
		"THIS ETEXT IS OTHERWISE painfully elaborate their projects",
		"Nevertheless the organization of most of the assemblies takes place",
		"With still greater boldness Montpellier enjoined all representatives",
		"\0" };


/**
* Metodo main da aplicacao
*/
int main(int argc, char *argv[])
{
	if (argc != 4)
	{
		ERROR("Nao e possivel executar o scrip com %d parametros, a sintaxe e: programa <arquivo> <hash> <tamanho_palavra>", argc);
	}

	cout << "-----------------------------" << endl;
	cout << "ARQUIVO: " << argv[1] << endl;
	cout << "HASH   : " << argv[2] << endl;
	cout << "PALAVRA: " << argv[3] << endl;
	cout << "-----------------------------" << endl;

	int palavra  = atoi(argv[3]);
	int hash    = atoi(argv[2]);
	const char* arquivo = argv[1];

	LeitorArquivo arq = LeitorArquivo(palavra, hash);
	IndiceInvertido* indice = arq.ler(arquivo);

	indice->imprimirPalavras(false);

	int i =0;
	while (termos[i][0] != '\0')
		indice->busca(string(termos[i++]));

	IMPRIME_ETAPAS()

	delete indice;
    return 0;
}
