#ifndef ARQUIVO_H
#define ARQUIVO_H

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
#include "indice_invertido.h"
#include "arquivo.h"
#include "debug.h"
#include "temporizador.h"

using namespace std;

class LeitorArquivo
{
    public:
		LeitorArquivo(int tamanhoPalavra);
		LeitorArquivo(int tamanhoPalavra, int tamanhoHash);
        virtual ~LeitorArquivo();
        IndiceInvertido* ler(const char* caminhoArquivo);
    private:
        unsigned int totalPalavras;
        unsigned int tamanhoPalavra;
        unsigned int tamanhoHash;
        void lerEntrada();
        void lerArquivo(Arquivo* arquivo, IndiceInvertido* tabelaHash);
        char* getPalavra(FILE* arquivo_entrada);
};


class ArquivoLista
{
    public:
       string nome;
       int indice;
    protected:

    private:
};

#endif
