#ifndef ARVORE_AVL_H_
#define ARVORE_AVL_H_

#include <algorithm>
#include <iostream>
#include "termo.h"
#include "arquivo.h"
#include "temporizador.h"
#include "debug.h"

using namespace std;


class OcorrenciaTermo
{
	public:
		OcorrenciaTermo(Termo* termo, Arquivo* arquivo);
		~OcorrenciaTermo();
		Termo* getTermo() const { return termo;};
		Arquivo* getArquivo() const {return arquivo;};
		OcorrenciaTermo* clone() const;
	private:
		Termo* termo;
		Arquivo* arquivo;
};

class Ocorrencia
{
	public:
		Ocorrencia(Arquivo* arquivo);
		Arquivo* getArquivo() const { return arquivo;};
		int getContagem() const { return contagem;};
		void inc();
	private:
		Arquivo* arquivo;
		int contagem;
};

class ListaOcorrencia
{
	public:
		ListaOcorrencia(Arquivo* arquivo);
		~ListaOcorrencia();
		Ocorrencia* getOcorrencia() const { return ocorrencia;};
		ListaOcorrencia* getProx() const {return prox;};
		void add(Arquivo* arquivo);
		unsigned int getTamanho() const {return tamanho;};
	private:
		Ocorrencia* ocorrencia;
		ListaOcorrencia* prox;
		unsigned int tamanho;
};

class ListaTermoOcorrencia {
	public:
		ListaTermoOcorrencia(Termo* t, ListaOcorrencia* o, ListaTermoOcorrencia* p)
		{
			termo = t; ocorrencias = o; prox = p;
		};
		Termo* getTermo() const { return termo; };
		ListaOcorrencia* getOcorrencias() const { return ocorrencias; };
		ListaTermoOcorrencia* getProx() const { return prox; };
	private:
		Termo* termo;
		ListaOcorrencia* ocorrencias;
		ListaTermoOcorrencia* prox;
};


class No
{
	public:
		No(OcorrenciaTermo* ocorrenciaTermo);
		~No();
		void setEsquerda(No* no);
		No* getEsquerda() const {return esquerda;};
		void setDireita(No* no);
		No* getDireita() const {return direita;};
		Termo* getTermo() const {return termo;};
		ListaOcorrencia* getOcorrencias() const {return ocorrencias;};
		void add(OcorrenciaTermo* ocorrenciaTermo);
		int getAltura() const {return altura;};
	private:
		Termo* termo;
		ListaOcorrencia* ocorrencias;
		No* esquerda;
		No* direita;
		int altura;
		void atualizaAltura();
};

class ArvoreAVL
{
    public:
		ArvoreAVL();
		bool insere(OcorrenciaTermo* ocorrenciaTermo);
		No* getRaiz() const {return raiz;};
		int getAltura() const;
		ListaOcorrencia* busca(Termo* t) const;
		void imprimir() const;
		void imprimir(int recuo) const;
		void imprimirPalavras(bool imprime) const;
    private:
		No* raiz;

		No* insereNo(No* raiz, OcorrenciaTermo* ocorrenciaTermos, bool& novo);
		int altura(No* temp);
        int diff(No* temp);
        No *rotacaoSimplesADireita(No* pai);
        No *rotacaoSimplesAEsquerda(No* pai);
        No* balancear(No* temp);
        No* rotacaoDireitaEsquerda(No* pai);
        No* rotacaoEsquerdaDireita(No* pai);
        void imprimir(No* raiz, int nivel) const;
        void imprimirPalavras(No* raiz, bool imprime) const;
};

#endif
