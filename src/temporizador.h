#ifndef TEMPORIZADOR_H_
#define TEMPORIZADOR_H_
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <ctime>
#include "debug.h"

class Temporizador;
extern Temporizador etapas[];

using namespace std;

enum  Etapa {
	CRIAR_INDICE_INVERTIDO,
	LEITURA_ARQUIVO,
	PARSE_PALAVRA,
	INSERE_HASH,
	CALCULO_HASH,
	INSERE_ARVORE,
	INSERE_ARVORE_PALAVRAS_ORDENADAS,
	COMPARA_TERMOS,
	BALANCEAMENTO_ARVORE,
	BUSCA_INDICE_INVERTIDO,
	BUSCA_ARVORE,
	LISTA_PALAVRAS,
	FIM
};

class Temporizador {
 public:
	Temporizador(Etapa id, int level,const char* label);
	~Temporizador();
	void inicio();
	void fim();
	void imprimir() const;
	double getTempoMedio() const;
	double getMenorTempo() const;
	double getMaiorTempo() const;
	Etapa getId() const { return id;};
private:
	Etapa id;
	int level;
	const char* titulo;
	clock_t ultimo_inicio;
	double tempo_total;
	double menor_tempo;
	double maior_tempo;
	long numero_chamadas;

};

void imprimirEtapas();
void imprimirEtapasTestes();

#define INICIO_ETAPA(etapa) etapas[etapa].inicio();
#define FIM_ETAPA(etapa) etapas[etapa].fim();
#define IMPRIME_ETAPAS() imprimirEtapas();
#define IMPRIME_ETAPAS_TESTES() imprimirEtapasTestes();

#endif
