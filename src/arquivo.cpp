#include "arquivo.h"

Arquivo::Arquivo(int id, string nome)
{
	Arquivo::id = id;
	Arquivo::nome = nome;
	Arquivo::totalPalavras = 0;
	Arquivo::totalDistintas = 0;
}

Arquivo::~Arquivo() {

}


void Arquivo::addPalavra(bool nova)
{
	totalPalavras++;
	if (nova)
		totalDistintas++;
}

bool ListaArquivo::add(Arquivo* arquivo)
{
	bool added = false;
	if (this->arquivo == NULL)
	{
		this->arquivo = arquivo;
		added = true;
	}
	else
	{
		ListaArquivo* it = this;
		while (it != NULL && it->getArquivo()->getId() != arquivo->getId())
		{
			it = it->getProx();
		}

		if (it == NULL)
		{
			ListaArquivo* aux = new ListaArquivo(getArquivo(), this->getProx());
			this->arquivo = arquivo;
			this->prox = aux;
			added = true;
		}
	}

	return added;
}
